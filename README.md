# Projeto de estudos REACT
Projeto com objetivo de aprofundamento e prática para desenvolvimento FRONTEND utilizando REACT

### GUIA PARA INSTALAÇÃO E CONFIGURAÇÃO DO AMBIENTE
Para este projeto utilizamos NODEJS, NPM OU YARN e REACT. Caso não tenha o ambiente configurado, utilize o seguinte link:
[Instalação e configuração](https://www.notion.so/Configura-es-do-ambiente-Node-js-ae9fea3f78894139af4268d198294e2a)

### CRIANDO O PROJETO
Nesta etapa iremos criar o nosso projeto REACT, para isso é necessário que você esteja com o ambiente configurado conforme o passo anterior. 
Caso tenha algum problema na instalação, informe o erro ou a dificuldade que está tendo para o pai [Google](https://www.google.com.br/) e tenha bastante fé e paciência.

Agora vamos ao passo a passo para nosso primeiro projeto.

1. Com o NODE e NPM ou YARN instalados, vamos criar a pasta **WORKSPACE** na raiz do computador **C:/**, isso é para organizarmos nosso ambiente de estudo ou trabalho, você pode criar a pasta em qualquer outro lugar ou outro nome.
2. Após criar a pasta **WORKSPACE**, abra o terminal de comando de sua preferencia (CMD, GIT BASH, VSCODE, ...) e navegue até a pasta.
3. Dentro da pasta, execute o seguinte comando:
```
    npm create vite@latest
```
4. Escolha o nome do seu projeto e pressione enter
5. Navegue com as setas do teclado até a opção **react** e pressione enter e selecione a primeira opção **react**
6. Após carregar a estrutura básica do seu projeto acesse a pasta do seu projeto
7. Execute o comando abaixo dentro da pasta do seu projeto para todas as dependencias
``` 
npm install 
```
8. Se você foi abençoado e não teve nenhuma mensagem de erro, você poderá executar o comando abaixo para visualizar no seu navegador o HTML basico do projeto
```
npm run dev
```